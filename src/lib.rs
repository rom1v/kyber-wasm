use log::debug;
use wasm_bindgen::prelude::*;

mod cell_buf;
mod demuxer;
mod demuxer_worker;
mod packet;
mod renderer;
mod renderer_worker;
mod video_decoder;
pub use demuxer_worker::{DemuxerWorker, DemuxerWorkerCtx};

#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    #[cfg(debug_assertions)]
    console_error_panic_hook::set_once();

    debug!("Wasm loaded");

    Ok(())
}

#[wasm_bindgen]
pub struct WasmCtx {
    document: web_sys::Document,
}

#[wasm_bindgen]
impl WasmCtx {
    pub fn new() -> Self {
        let window = web_sys::window().expect("no global `window` exists");
        let document = window.document().expect("should have a document on window");
        Self { document }
    }

    pub fn connect(&mut self) -> Result<(), JsValue> {
        let url = self
            .document
            .get_element_by_id("url")
            .expect("No url element")
            .dyn_into::<web_sys::HtmlInputElement>()?
            .value();
        debug!("url={url}");

        let canvas = self
            .document
            .get_element_by_id("canvas")
            .expect("No canvas element")
            .dyn_into::<web_sys::HtmlCanvasElement>()?;

        let offscreen_canvas = canvas.transfer_control_to_offscreen()?;

        let worker = DemuxerWorker::new()?;
        worker.start(&url, offscreen_canvas)?;

        Ok(())
    }
}
