use wasm_bindgen::prelude::*;

pub(crate) struct Renderer {
    canvas: web_sys::OffscreenCanvas,
    ctx: web_sys::OffscreenCanvasRenderingContext2d,
}

impl Renderer {
    pub(crate) fn new(canvas: web_sys::OffscreenCanvas) -> Result<Self, JsValue> {
        let ctx = canvas
            .get_context("2d")?
            .ok_or(JsValue::from(js_sys::Error::new("No 2D context")))?
            .dyn_into::<web_sys::OffscreenCanvasRenderingContext2d>()?;
        Ok(Self { canvas, ctx })
    }

    pub(crate) fn draw(&self, frame: web_sys::VideoFrame) -> Result<(), JsValue> {
        let width = frame.display_width();
        let height = frame.display_height();
        self.canvas.set_width(width);
        self.canvas.set_height(height);
        self.ctx.draw_image_with_video_frame_and_dw_and_dh(
            &frame,
            0.0,
            0.0,
            width as f64,
            height as f64,
        )?;
        frame.close();
        Ok(())
    }
}
