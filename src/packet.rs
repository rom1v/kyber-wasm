#[derive(Debug)]
pub(crate) enum Packet {
    Codec(CodecPacket),
    Media(MediaPacket),
}

#[derive(Debug)]
#[allow(dead_code)]
pub(crate) struct CodecPacket {
    // don't care for now
}

#[derive(Debug)]
#[allow(dead_code)]
pub(crate) struct MediaPacket {
    pub(crate) header: MediaPacketHeader,
    pub(crate) data: js_sys::Uint8Array, // does not include the 12-byte header
}

#[derive(Debug)]
pub(crate) struct MediaPacketHeader {
    pub(crate) is_config: bool,
    pub(crate) is_key: bool,
    pub(crate) pts: u64,
    pub(crate) size: u32,
}
