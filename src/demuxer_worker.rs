use crate::demuxer::Demuxer;
use crate::renderer_worker::RendererWorker;
use crate::video_decoder::VideoDecoderCallbacks;
use log::debug;
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::{spawn_local, JsFuture};

struct CloseCallbacks {
    then: Closure<dyn FnMut(JsValue)>,
    catch: Closure<dyn FnMut(JsValue)>,
}

#[wasm_bindgen]
pub struct DemuxerWorkerCtx {
    close_cbs: Option<CloseCallbacks>,
}

#[wasm_bindgen]
impl DemuxerWorkerCtx {
    pub fn new() -> Self {
        //let _global = js_sys::global()
        //    .dyn_into::<web_sys::DedicatedWorkerGlobalScope>()
        //    .expect("No worker global scope");

        debug!("Hello from worker in Wasm");
        Self { close_cbs: None }
    }

    pub async fn on_message(&mut self, event: web_sys::MessageEvent) -> Result<(), JsValue> {
        let msg = &event.data().dyn_into::<js_sys::Object>()?;
        let url = js_sys::Reflect::get(&msg, &"url".into())?
            .as_string()
            .expect("No url");
        let canvas =
            js_sys::Reflect::get(&msg, &"canvas".into())?.dyn_into::<web_sys::OffscreenCanvas>()?;
        self.connect(&url, canvas).await
    }

    pub async fn connect(
        &mut self,
        url: &str,
        canvas: web_sys::OffscreenCanvas,
    ) -> Result<(), JsValue> {
        let web_transport = web_sys::WebTransport::new(url)?;
        debug!("Initiating connection...");
        JsFuture::from(web_transport.ready()).await?;
        debug!("Connection ready");

        let then = Closure::once(move |_| {
            debug!("Connection closed");
        });
        let catch = Closure::once(move |_| {
            debug!("Connection aborted");
        });

        // Keep the closures alive
        self.close_cbs = Some(CloseCallbacks { then, catch });
        let cbs = self.close_cbs.as_ref().unwrap();
        let _ = web_transport.closed().then2(&cbs.then, &cbs.catch);

        spawn_local(async move {
            Self::accept_uni_stream(&web_transport, canvas)
                .await
                .unwrap_throw();
        });

        Ok(())
    }

    async fn accept_uni_stream(
        web_transport: &web_sys::WebTransport,
        canvas: web_sys::OffscreenCanvas,
    ) -> Result<(), JsValue> {
        let unistreams_reader = web_transport
            .incoming_unidirectional_streams()
            .get_reader()
            .dyn_into::<web_sys::ReadableStreamDefaultReader>()?;

        let obj = JsFuture::from(unistreams_reader.read()).await?;
        let done = js_sys::Reflect::get(&obj, &JsValue::from("done"))?
            .as_bool()
            .unwrap_or(false);
        if done {
            return Err(JsValue::from("No unistream"));
        }

        let stream = js_sys::Reflect::get(&obj, &JsValue::from("value"))?
            .dyn_into::<web_sys::ReadableStream>()?;

        debug!("Unistream opened");

        let renderer_worker = RendererWorker::new()?;
        renderer_worker.start(canvas)?;

        let cbs = VideoDecoderCallbacks {
            output: Box::new(move |frame| {
                debug!(
                    "video frame received from the decoder {}x{}",
                    frame.coded_width(),
                    frame.coded_height()
                );
                renderer_worker.send_frame(frame)?;
                Ok(())
            }),
            error: Box::new(move |error| {
                debug!("video decoding error: {error:?}");
                Ok(())
            }),
        };

        let mut demuxer = Demuxer::new(stream);
        demuxer.demux_loop(cbs).await
    }
}

pub struct DemuxerWorker {
    worker: web_sys::Worker,
}

impl DemuxerWorker {
    pub fn new() -> Result<DemuxerWorker, JsValue> {
        let worker = web_sys::Worker::new_with_options(
            "./demuxer_worker.js",
            web_sys::WorkerOptions::new().type_(web_sys::WorkerType::Module),
        )?;
        Ok(Self { worker })
    }

    pub fn start(&self, url: &str, canvas: web_sys::OffscreenCanvas) -> Result<(), JsValue> {
        let msg = js_sys::Object::new();
        let ok = js_sys::Reflect::set(&msg, &"url".into(), &url.into())?;
        assert!(ok);

        let canvas_param = canvas.into();
        let ok = js_sys::Reflect::set(&msg, &"canvas".into(), &canvas_param)?;
        assert!(ok);

        let transfer = js_sys::Array::of1(&canvas_param);

        self.worker
            .post_message_with_transfer(&msg, &transfer.into())?;

        Ok(())
    }
}
