use crate::cell_buf::{self, CellBufConsumer, CellBufProducer};
use crate::renderer::Renderer;
use log::debug;
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::spawn_local;

#[wasm_bindgen]
pub struct RendererWorkerCtx {
    producer: Option<CellBufProducer<web_sys::VideoFrame>>,
}

#[wasm_bindgen]
impl RendererWorkerCtx {
    pub fn new() -> Self {
        Self {
            producer: None,
        }
    }

    pub fn on_message(&mut self, event: web_sys::MessageEvent) -> Result<(), JsValue> {
        let msg = &event.data().dyn_into::<js_sys::Object>()?;

        let msg_type = js_sys::Reflect::get(&msg, &"type".into())?
            .as_string()
            .expect("No message type");

        match msg_type.as_str() {
            RendererWorker::TYPE_START => {
                assert!(self.producer.is_none());
                let canvas = js_sys::Reflect::get(&msg, &"canvas".into())?
                    .dyn_into::<web_sys::OffscreenCanvas>()?;
                let (producer, consumer) = cell_buf::cell_buf();
                self.producer = Some(producer);
                spawn_local(async move {
                    Self::display_frames(canvas, consumer).await.unwrap_throw();
                });
            }
            RendererWorker::TYPE_FRAME => {
                assert!(self.producer.is_some());
                let frame = js_sys::Reflect::get(&msg, &"frame".into())?
                    .dyn_into::<web_sys::VideoFrame>()?;
                self.push(frame)?;
            }
            _ => {
                return Err(JsValue::from(format!(
                    "Unexpected message type: {msg_type}"
                )));
            }
        }

        Ok(())
    }

    fn push(&mut self, frame: web_sys::VideoFrame) -> Result<(), JsValue> {
        debug!(
            "video frame to display {}x{}",
            frame.coded_width(),
            frame.coded_height()
        );
        let old = self.producer.as_mut().expect("No producer").replace(frame);
        if let Some(old) = old {
            // Previous frame skipped
            debug!("Frame skipped");
            old.close();
        }
        Ok(())
    }

    async fn display_frames(
        canvas: web_sys::OffscreenCanvas,
        consumer: CellBufConsumer<web_sys::VideoFrame>,
    ) -> Result<(), JsValue> {
        let renderer = Renderer::new(canvas)?;
        loop {
            let frame = consumer.take().await;
            renderer.draw(frame)?;
        }
    }
}

pub struct RendererWorker {
    worker: web_sys::Worker,
}

impl RendererWorker {
    const TYPE_START: &'static str = "start";
    const TYPE_FRAME: &'static str = "frame";

    pub fn new() -> Result<RendererWorker, JsValue> {
        let worker = web_sys::Worker::new_with_options(
            "./renderer_worker.js",
            web_sys::WorkerOptions::new().type_(web_sys::WorkerType::Module),
        )?;
        Ok(Self { worker })
    }

    pub fn start(&self, canvas: web_sys::OffscreenCanvas) -> Result<(), JsValue> {
        let msg = js_sys::Object::new();

        let ok = js_sys::Reflect::set(&msg, &"type".into(), &Self::TYPE_START.into())?;
        assert!(ok);

        let canvas_param = canvas.into();
        let ok = js_sys::Reflect::set(&msg, &"canvas".into(), &canvas_param)?;
        assert!(ok);

        let transfer = js_sys::Array::of1(&canvas_param);

        self.worker
            .post_message_with_transfer(&msg, &transfer.into())?;

        Ok(())
    }

    pub fn send_frame(&self, frame: web_sys::VideoFrame) -> Result<(), JsValue> {
        let msg = js_sys::Object::new();

        let ok = js_sys::Reflect::set(&msg, &"type".into(), &Self::TYPE_FRAME.into())?;
        assert!(ok);

        let frame_param = frame.into();
        let ok = js_sys::Reflect::set(&msg, &"frame".into(), &frame_param)?;
        assert!(ok);

        let transfer = js_sys::Array::of1(&frame_param);

        self.worker
            .post_message_with_transfer(&msg, &transfer.into())?;

        Ok(())
    }
}
