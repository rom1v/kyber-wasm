use crate::packet::{CodecPacket, MediaPacket, MediaPacketHeader, Packet};
use crate::video_decoder::{VideoDecoder, VideoDecoderCallbacks};
use bytes::{Buf, BufMut, BytesMut};
use js_sys::Uint8Array;
use log::debug;
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::JsFuture;

pub(crate) struct Demuxer {
    stream: web_sys::ReadableStream,
    video_decoder: Option<VideoDecoder>,
}

impl Demuxer {
    pub(crate) fn new(stream: web_sys::ReadableStream) -> Self {
        Self {
            stream,
            video_decoder: None,
        }
    }

    pub(crate) async fn demux_loop(&mut self, cbs: VideoDecoderCallbacks) -> Result<(), JsValue> {
        let reader = self
            .stream
            .get_reader()
            .dyn_into::<web_sys::ReadableStreamDefaultReader>()?;

        let mut cbs = Some(cbs);

        let mut packetizer = Packetizer::new();
        loop {
            let obj = JsFuture::from(reader.read()).await?;
            let done = js_sys::Reflect::get(&obj, &JsValue::from("done"))?
                .as_bool()
                .unwrap_or(false);
            if done {
                debug!("Unistream closed");
                if let Some(video_decoder) = &self.video_decoder {
                    video_decoder.flush().await?;
                }
                return Ok(());
            }

            // value is a JsValue(Uint8Array)
            let value = js_sys::Reflect::get(&obj, &JsValue::from("value"))?
                .dyn_into::<js_sys::Uint8Array>()?;

            let chunk = value.to_vec();
            packetizer.write(&chunk);
            while let Some(packet) = packetizer.read_packet() {
                match packet {
                    Packet::Media(packet) => {
                        assert!(self.video_decoder.is_some());
                        if let Some(video_decoder) = &mut self.video_decoder {
                            debug!(
                                "media packet {} {} {} {}",
                                packet.header.is_config,
                                packet.header.is_key,
                                packet.header.pts,
                                packet.header.size
                            );
                            video_decoder.send_packet(packet)?;
                        } else {
                            panic!("Video decoder not set");
                        }
                    }
                    Packet::Codec(CodecPacket {}) => {
                        assert!(self.video_decoder.is_none());
                        debug!("codec packet");
                        self.video_decoder = Some(VideoDecoder::new(
                            cbs.take().expect("Callbacks already consumed"),
                        )?);
                    }
                }
            }
        }
    }
}

pub(crate) struct Packetizer {
    buf: BytesMut,
    sid: Option<u16>,
}

impl Packetizer {
    pub fn new() -> Self {
        Self {
            buf: BytesMut::with_capacity(0x10000),
            sid: None,
        }
    }

    pub fn write(&mut self, chunk: &[u8]) {
        if chunk.len() > self.buf.capacity() - self.buf.len() {
            // Increase the buffer size
            self.buf.reserve(self.buf.len() + chunk.len());
        }
        self.buf.put_slice(chunk);
    }

    pub fn read_packet(&mut self) -> Option<Packet> {
        if self.sid.is_none() {
            if self.buf.len() < 2 {
                return None;
            }

            let sid = self.buf.get_u16();
            debug!("sid = {sid:X}");
            self.sid = Some(sid);
        }

        if self.buf.len() < 12 {
            // Not enough data (the header is incomplete)
            return None;
        }

        let is_media_packet = self.buf[0] & 0x80 != 0;
        if is_media_packet {
            let size = u32::from_be_bytes(self.buf[8..12].try_into().unwrap());
            let kypacket_size = 12 + size as usize;
            if self.buf.len() < kypacket_size {
                // Not enough payload data
                return None;
            }

            let pts_and_flags = u64::from_be_bytes(self.buf[..8].try_into().unwrap());
            let is_config = (pts_and_flags & 0x40_00_00_00_00_00_00_00) != 0;
            let is_key = (pts_and_flags & 0x20_00_00_00_00_00_00_00) != 0;
            let pts = pts_and_flags & 0x1F_FF_FF_FF_FF_FF_FF_FF;

            let header = MediaPacketHeader {
                is_config,
                is_key,
                pts,
                size,
            };

            let slice = &self.buf[12..kypacket_size];
            let data = Uint8Array::from(slice);

            self.buf.advance(kypacket_size);

            Some(Packet::Media(MediaPacket { header, data }))
        } else {
            self.buf.advance(12);
            Some(Packet::Codec(CodecPacket {}))
        }
    }
}
