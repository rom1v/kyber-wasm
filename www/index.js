import init, { WasmCtx } from "./pkg/kyber_wasm.js";
await init();

self.wasm = WasmCtx.new();
